class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_parent
    if session[:parent_id].blank?
      nil
    else
      Parent.where(id: session[:parent_id]).first
    end
  end
  def current_kid
    if session[:kid_id].blank?
      nil
    else
      Kid.where(id: session[:kid_id]).first
    end
  end
  helper_method :current_parent
  helper_method :current_kid


end
