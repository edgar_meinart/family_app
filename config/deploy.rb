lock '3.2.1'

set :application, 'family_app'
set :repo_url, 'git@bitbucket.org:edgar_meinart/website.git'
set :deploy_to, "/home/web/apps/#{fetch(:application)}"

set :keep_releases, 3

set :scm, :git

set :rvm_ruby_string, :local


namespace :nginx do
  desc "Stop NGINX"
  task :stop do
    on roles(:web) do
      sudo '/etc/init.d/nginx stop'
    end
  end

  desc "Start NGINX"
  task :start do
    on roles(:web) do
      sudo '/etc/init.d/nginx start'
    end
  end

  desc "Reload NGINX"
  task :reload do
    on roles(:web) do
      sudo '/etc/init.d/nginx reload'
    end
  end
end

namespace :deploy do

  before :finished, 'newrelic:notice_deployment'

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join("#{fetch(:deploy_to)}/shared/restart.txt")
    end
  end
end
