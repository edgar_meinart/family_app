ParentApp::Application.routes.draw do

  # get '/auth/:provider/callback', to: 'sessions#create'

  # get 'kid_login', to: 'kids#login'
  # get 'kid_logout', to: 'kids#logout'
  # get 'show_task', to: 'kids#show_task'
  # get 'submit_task', to: 'kids#submit_task'
  # get 'show_tasklist', to: 'kids#show_tasklist'
  # post 'kid_create_session', to: 'kids#create_session'

  # resources :kids, only: [ :show, :new, :create ] do
  #   get :login
  #   get :logout
  #   get :assign
  # end
  # get 'login', to: 'parents#login'
  # get 'logout', to: 'parents#logout'

  # resources :sessions, only: [ :create]
  # # namespace :mobile do
  # resources :parents, only: [ :show ] do

  #   resources :task_lists, only: [ :new, :create, :show, :destroy ] do
  #     resources :tasks, only: [:index, :new, :create, :show, :destroy ]
  #     resources :prizes, only: [:index, :new, :create, :show ]
  #   end
  # end
  # # end

  root to: 'home#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
